﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Senacao.Data;
using Senacao.Droid;
using SQLite;

[assembly: Xamarin.Forms
    .Dependency(typeof(SQLite_android))]
namespace Senacao.Droid
{
    class SQLite_android : ISQLite
    {
        private const string
            arquivoDb = "Senacao.db3";

        public SQLiteConnection Conexao()
        {
            var pathDb =
                Path.Combine(Android.OS
                .Environment.ExternalStorageDirectory
                .Path, arquivoDb);

            return new SQLite.SQLiteConnection
                (pathDb);
        }
    }
}