﻿using Senacao.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Senacao.Data
{
    class AgendamentoDAO
    {
        readonly SQLiteConnection Conexao;

        public AgendamentoDAO(SQLiteConnection conexao)
        {
            this.Conexao = conexao;
            this.Conexao.CreateTable<Agendamento>();
        }

        public void Salvar(Agendamento agendamento)
        {
            Conexao.Insert(agendamento);
        }

        public List<Agendamento> ListarAgendamento()
        {
            return Conexao.Table<Agendamento>().ToList();
        }


    }
}
